const {GraphQLServer, PubSub} = require('graphql-yoga');
const { ApolloServer } = require('apollo-server');
const Subscription = require("./resolvers/Subscriptions");

// Some fake data
const contacts = [
    {
        id: 1,
        firstName: "Bugariu",
        lastName: "Bogdan",
        notes: [
            {
                id: "2",
                detail: "Notes Detail 1"
            },
            {
                id: "1",
                detail: "Notes Detail 2"
            },
        ]
    },
    {
        id: 2,
        firstName: "Bugariu",
        lastName: "Catalin",
        notes: [
            {
                id: "3",
                detail: "Notes Detail 1"
            },
            {
                id: "4",
                detail: "Notes Detail 2"
            },
        ]
    },
    {
        id: 3,
        firstName: "Bugariu",
        lastName: "Mihai",
        notes: [
            {
                id: "5",
                detail: "Notes Detail 1"
            },
            {
                id: "6",
                detail: "Notes Detail 2"
            },
        ]
    },
    {
        id: 4,
        firstName: "Leca Sucvinderzing",
        lastName: "Horatiu",
        notes: [
            {
                id: "7",
                detail: "Notes Detail 1"
            },
            {
                id: "8",
                detail: "Notes Detail 2"
            },
        ]
    },
];

// The resolvers
const resolvers = {
    Query: {
        contacts: () => contacts,
        contact: (parent, args) => contacts.find(contact => contact.id === Number(args.id))
    },
    Mutation: {
        addContact: (parent, args, context, info) => {
            const newContact = {
                id: contacts.length + 1,
                firstName: args.firstName,
                lastName: args.lastName
            }

            context.pubSub.publish("CONTACT_CREATED", newContact)

            contacts.push(newContact)
            return newContact;
        },

        addNote: (parent, args, context, info) => {
            const newNote = {
                id: Math.random().toString(5),
                detail: args.note.detail
            }

            const contact = contacts.find(contact => contact.id === Number(args.note.contactId));

            contact.notes.push(newNote);
            context.pubSub.publish("NOTE_ADDED", newNote);

            return newNote;
        }
    },

    Subscription
};

const pubSub = new PubSub()

const server2= new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers,
    context: request => ({
        ...request,
        pubSub
    }),
})

// const server = new ApolloServer({
//     typeDefs: './src/schema.graphql',
//     resolvers,
// })

server2.subscriptionServer(async () => await console.log(`Server is running on http://localhost:4000`));
