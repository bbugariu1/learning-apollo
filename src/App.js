import React from 'react';
import './App.css';
import ListContacts from "./Contacts/ListContacts";
import {ApolloClient, ApolloProvider, createHttpLink, InMemoryCache} from "@apollo/client";
import AddContact from "./Contacts/AddContact";

const httpLink = createHttpLink({
    uri: "http://localhost:4000/"
})

const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache()
});

function App() {
    return (
        <ApolloProvider client={client}>
            <div className="App">
                <header className="App-header">
                    <h2>CRM</h2>
                    <AddContact />
                    <ListContacts/>
                </header>
            </div>
        </ApolloProvider>
    );
}

export default App;
