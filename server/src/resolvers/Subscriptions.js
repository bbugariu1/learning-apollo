function addNewNoteSubscription (parent, args, context) {
    return context.pubSub.asyncIterator("NOTE_ADDED")
}

const noteAdded = {
    subscribe: addNewNoteSubscription,
    resolve: payload => payload
}

module.exports = {
    noteAdded
}