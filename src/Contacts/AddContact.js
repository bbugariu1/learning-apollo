import React from "react";
import {gql} from "@apollo/client";
import {Mutation} from "@apollo/client/react/components";
import {CONTACTS_LIST_QUERY} from "./ListContacts";

const AddContact = () => {
    const [firstName, setFirstName] = React.useState("");
    const [lastName, setLastName] = React.useState("");

    const _updateList = (store, contact) => {
        const data = store.readQuery({query: CONTACTS_LIST_QUERY});

        store.writeQuery({
            query: CONTACTS_LIST_QUERY, data: {
                ...data,
                contacts: [...data.contacts, contact]
            }
        })
    }

    return (
        <div>
            <input type="text" placeholder={"first name"} onChange={e => setFirstName(e.target.value)}/>
            <input type="text" placeholder={"last name"} onChange={e => setLastName(e.target.value)}/>
            <Mutation
                mutation={ADD_CONTACT_MUTATION}
                variables={{firstName, lastName}}
                update={(store, {data: {addContact}}) => (
                    _updateList(store, addContact)
                )}>
                {mutation => (
                    <button onClick={mutation}>Add Contact</button>
                )}
            </Mutation>

        </div>
    );
}

export const ADD_CONTACT_MUTATION = gql`
    mutation addContact($firstName: String!, $lastName: String!){
        addContact(firstName: $firstName, lastName: $lastName) {
            id
            firstName
            lastName
        }
    }
`;
export default AddContact