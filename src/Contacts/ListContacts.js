import React from "react";
import {gql, useQuery} from "@apollo/client";

const ListContacts = () => {
    const {loading, error, data} = useQuery(CONTACTS_LIST_QUERY);

    if (loading) {
        return <p>Loading ...</p>
    }

    if (error) {
        return <p>{error.message}</p>
    }

    return (
        <ul>
            {data.contacts.map(contact => (
                <li key={contact.id}>{contact.firstName} {contact.lastName}</li>
            ))}
        </ul>
    );
}


export const CONTACTS_LIST_QUERY = gql`
    query ContactsQuery {
        contacts {
            id
            firstName
            lastName
        }
    }
`;
export default ListContacts;